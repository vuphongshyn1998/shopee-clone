import React from 'react'
import Header from '../../components/header'
import Footer from '../../components/footer'

interface Pros {
  children?: React.ReactNode
}

export default function MainLayout({ children }: Pros) {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  )
}
