import Footer from '../../components/footer'
import RegisterHeader from '../../components/register-header'

interface Pros {
  children?: React.ReactNode
}

export default function RegisterLayout({ children }: Pros) {
  return (
    <div>
      <RegisterHeader />
      {children}
      <Footer />
    </div>
  )
}
