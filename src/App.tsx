import useRouterElements from './useRouterElements'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function App() {
  const routerElentments = useRouterElements()
  return (
    <div>
      {routerElentments}
      <ToastContainer />
    </div>
  )
}

export default App
