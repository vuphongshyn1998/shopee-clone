import { useQuery } from '@tanstack/react-query'
import AsindeFilter from './components/aside-filter'
import Product from './components/product'
import SortProducts from './components/sort-product-list'
import useQueryParams from '../../hooks/useQueryParam'
import productApi from '../../apis/product.api'
import Pagination from '../../components/pagination'
import { ProductListConfig } from '../../types/product.type'
import { isUndefined, omitBy } from 'lodash'
import categoryApi from '../../apis/category.api'

export type QueryConfig = {
  [key in keyof ProductListConfig]: string
}

export default function ProductList() {
  const queryParams: QueryConfig = useQueryParams()
  const queryConfig: QueryConfig = omitBy(
    {
      page: queryParams.page || '1',
      limit: queryParams.limit,
      sort_by: queryParams.sort_by,
      order: queryParams.order,
      exclude: queryParams.exclude,
      rating_filter: queryParams.rating_filter,
      price_max: queryParams.price_max,
      price_min: queryParams.price_min,
      name: queryParams.name,
      category: queryParams.category
    },
    isUndefined
  )

  const { data: products } = useQuery({
    queryKey: ['products', queryParams],
    queryFn: () => {
      return productApi.getProducts(queryConfig as ProductListConfig)
    },
    keepPreviousData: true
  })

  const { data: categories } = useQuery({
    queryKey: ['categories'],
    queryFn: () => {
      return categoryApi.getCategories()
    },
    keepPreviousData: true
  })

  return (
    <div className='bg-gray-200 py-6'>
      <div className='container'>
        {products && (
          <div className='grid grid-cols-12 gap-6'>
            <div className='col-span-3'>
              <AsindeFilter queryConfig={queryConfig} categories={categories?.data.data || []} />
            </div>
            <div className='col-span-9'>
              <SortProducts queryConfig={queryConfig} pageSize={products?.data.data.pagination.page_size} />
              <div className='mt-6 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-3'>
                {products &&
                  products.data.data.products.map((product) => (
                    <div className='col-span-1' key={product._id}>
                      <Product product={product} />
                    </div>
                  ))}
              </div>
              <Pagination queryConfig={queryConfig} pageSize={products?.data.data.pagination.page_size} />
            </div>
          </div>
        )}
      </div>
    </div>
  )
}
